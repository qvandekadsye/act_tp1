package classe;

import java.util.ArrayList;

public class Outils {

	/**
	 * Cette fonction représente le principe de fusion des lignes de toit.
	 * Cependant, nous n'arrivons pas à obtenir le résultat escomté, une conditon nous échappant.
	 * Nous sommes pourtant proche du résultat.
	 * @param t1 Le premier tableau.
	 * @param t2 Le deuxième tableau
	 * @return Le tableau représentant les lignes de toit fusionnées.
	 */
	public static Point[] FusionLigneToit(Point[] t1,Point[] t2 )
	{
		Point[] tFinal= new Point[t1.length+t2.length]; //Le tableau que l'on renvoie
		int n = Math.max(t1.length, t2.length);// Calcul du tableau le plus long
		int indice = 0; //L'indice permet de savoir à quel emplacement du tableau final, ajouter les points.
		
		for(int i =0; i<n;i++)
		{
			if(t1[i].y<t2[i].y || t2[i-1].y < t1[i].y)
			{
				if(i<1)
				{
					tFinal[indice]=t1[i];
					indice++;
					tFinal[indice]=t2[i];
					indice++;
				}
				else
				{
					
						tFinal[indice]=t1[i];
						indice++;
						tFinal[indice]=t2[i];
						indice++;
					
					
					
				}
				
			}
			else if(t2[i-1].y > t1[i].y && t1[i].y >= t2[i].y) 
			{
				tFinal[indice]= new Point(Math.max(t1[i].x, t2[i].x), Math.max(t1[i].y, t2[i].y));
				indice++;
			}
			else if(t2[i-1].y < t1[i].y && t1[i].y > t2[i].y)
				tFinal[indice] = t1[i];
			
		}
		Point[] Tmax =(t1.length>t2.length)? t1:t2;
		
		return tFinal;
	}
	
	public void generateSkyline(Immeuble[] listeImmeuble)
	{
		//A partir de la liste des immeubles. On génère un tableau de Point.
		//On appelle récursivement la fonction FusionLigne de toit.
		
	}
}
