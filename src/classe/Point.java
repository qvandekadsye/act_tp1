package classe;

public class Point {
	protected int x,y;
	
	/**
	 * @param x L'abscisse du point.
	 * @param y L'ordonnée du point.
	 */
	public Point(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public void print()
	{
		
		System.out.print("("+this.x+","+this.y+") ");
	}

}
